#pragma once

#include "Sim.h"
#include "FluidSimEngine.h"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

class SimSingleLJPotential : public Sim
{
public:
    SimSingleLJPotential(FluidSimEngine*, const std::string&, const size_t);
    ~SimSingleLJPotential() override;
    void HandleTap(const int, const int) override;
    void HandleKey(const sf::Event::KeyEvent&) override;
    bool Update(float) override;
    void Render(sf::RenderWindow&, float) override;

private:
    void compute_forces();
    void ResetParticle(size_t);

    sf::RectangleShape bounds;
    sf::VertexArray vertices;
    size_t n;
    float* x;
    float* v;
    float* a;
};
