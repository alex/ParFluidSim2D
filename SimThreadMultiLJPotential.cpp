#include "SimThreadMultiLJPotential.h"

#include "LennardJones.h"
#include "Sim.h"

#include <SFML/Window.hpp>

#include <condition_variable>
#include <mutex>
#include <string>
#include <thread>
#include <type_traits>

#include <math.h>
#include <stdio.h>

SimThreadMultiLJPotential::SimThreadMultiLJPotential(FluidSimEngine* engine, const std::string& name, size_t count, const size_t num_th)
    : Sim(engine, name)
    , wait_flag(0)
    , num_threads(num_th)
    , bounds(sf::Vector2f(200.f, 230.f))
    , vertices(sf::Points, count)
    , n(count)
    , x(new float[2 * count])
    , v(new float[2 * count])
    , a(new float[2 * count])
    , aLocal(new float*[num_th])
    , terminateFlag(false)
    , timestep_count(0ULL)
    , timestep_rendered(0ULL)
{
    bounds.setFillColor(sf::Color(16, 16, 16));
    bounds.setPosition(300.f, 240.f);

    for (size_t i = 0; i < n; ++i) {
        ResetParticle(i);
    }

    for (size_t i = 0; i < num_threads; ++i) {
        aLocal[i] = new float[2 * count];
    }

    for (size_t i = 0; i < num_threads; ++i) {
        printf("[\033[32mSIM:C4\033[0m][COMPUTEF]: Starting compute thread %zu.\n", i);
        if (i)
            calc_threads.push_back(std::thread(&SimThreadMultiLJPotential::Compute, this, i));
        else
            calc_threads.push_back(std::thread(&SimThreadMultiLJPotential::ComputeMaster, this, i));
    }
}

SimThreadMultiLJPotential::~SimThreadMultiLJPotential()
{
    terminateFlag = true;

    for (size_t i = 0; i < num_threads; ++i)
        calc_threads[i].join();

    for (size_t i = 0; i < num_threads; ++i) {
        delete[] aLocal[i];
    }
    delete[] aLocal;

    delete[] x;
    delete[] v;
    delete[] a;
}

void SimThreadMultiLJPotential::HandleTap(const int x, const int y)
{ }

void SimThreadMultiLJPotential::HandleKey(const sf::Event::KeyEvent& key)
{
    if (key.code == sf::Keyboard::Backspace) {
        this->engine->PopSim();
    }
}

bool SimThreadMultiLJPotential::Update(float delta)
{
    // update the position of the corresponding vertex
    for (size_t i = 0; i < n; ++i) {
        vertices[i].position.x = x[2 * i + 0];
        vertices[i].position.y = x[2 * i + 1];
    }

    return timestep_count > timestep_rendered;
}

void SimThreadMultiLJPotential::Render(sf::RenderWindow& window, float delta)
{
    timestep_rendered = timestep_count;
    window.draw(bounds);
    window.draw(vertices);
}

inline size_t calc_work_begin(size_t n, size_t t, size_t i) {
    size_t remainder = n % t;

    return std::min(i, remainder) + n / t * i;
}

inline size_t calc_work_end(size_t n, size_t t, size_t i) {
    size_t remainder = n % t;

    return std::min(i + 1, remainder) + n / t * (i + 1);
}

void SimThreadMultiLJPotential::Compute(const size_t threadIdx)
{
    size_t begin = calc_work_begin(n, num_threads, threadIdx);
    size_t end = calc_work_end(n, num_threads, threadIdx);
    printf("[\033[32mSIM:C%zu\033[0m][\033[31mThread %zu\033[0m]: Worker thread does [%zu, %zu).\n", num_threads, threadIdx, begin, end);

    for (timestep_count = 0; !terminateFlag; ++timestep_count) {
        /* LEAPFROG 1 */

        {
            std::unique_lock<std::mutex> lk(cv_m);
            cv.wait(lk, [this] { return wait_flag & 0x01; });
        }

        leapfrog1(end - begin, 0.01, x + 2 * begin, v + 2 * begin, a + 2 * begin);

        {
            std::lock_guard<std::mutex> lk(cv_m);
            wait_flag |= (1ULL << threadIdx);
        }
        cv.notify_all();

        /* APPLY REFLECT */

        {
            std::unique_lock<std::mutex> lk(cv_m);
            cv.wait(lk, [this] { return !(wait_flag & 0x01); });
        }

        apply_reflect(end - begin, x + 2 * begin, v + 2 * begin, a + 2 * begin);

        {
            std::lock_guard<std::mutex> lk(cv_m);
            wait_flag &= ~(1ULL << threadIdx);
        }
        cv.notify_all();

        /* COMPUTE */

        {
            std::unique_lock<std::mutex> lk(cv_m);
            cv.wait(lk, [this] { return wait_flag & 0x01; });
        }

        for (size_t i = 0; i < n; ++i) {
            aLocal[threadIdx][2 * i + 0] = 0.f;
            aLocal[threadIdx][2 * i + 1] = 0.f;
        }
        /* Particle-particle interactions (Lennard-Jones) */
        for (size_t i = threadIdx; i < n; i += num_threads) {
            for (size_t j = i + 1; j < n; ++j) {
                float dx = x[2 * j + 0] - x[2 * i + 0];
                float dy = x[2 * j + 1] - x[2 * i + 1];
                float C_LJ = compute_LJ_scalar(dx * dx / 1000 + dy * dy / 1000, LJ_EPS, LJ_SIG2);
                if (isnan(C_LJ)) C_LJ = 0;
                if (C_LJ > 100) C_LJ = 100;
                if (C_LJ < -100) C_LJ = -100;
                aLocal[threadIdx][2 * i + 0] += (C_LJ * dx);
                aLocal[threadIdx][2 * i + 1] += (C_LJ * dy);
                aLocal[threadIdx][2 * j + 0] -= (C_LJ * dx);
                aLocal[threadIdx][2 * j + 1] -= (C_LJ * dy);
            }
        }

        {
            std::lock_guard<std::mutex> lk(cv_m);
            wait_flag |= (1ULL << threadIdx);
        }
        cv.notify_all();

        /* LEAPFROG 2 */

        {
            std::unique_lock<std::mutex> lk(cv_m);
            cv.wait(lk, [this] { return !(wait_flag & 0x01); });
        }

        leapfrog2(end - begin, 0.01, v + 2 * begin, a + 2 * begin);

        {
            std::lock_guard<std::mutex> lk(cv_m);
            wait_flag &= ~(1ULL << threadIdx);
        }
        cv.notify_all();

    }
}

void SimThreadMultiLJPotential::ComputeMaster(const size_t threadIdx)
{
    size_t begin = calc_work_begin(n, num_threads, threadIdx);
    size_t end = calc_work_end(n, num_threads, threadIdx);
    printf("[\033[32mSIM:C%zu\033[0m][\033[91mThread %zu\033[0m]: Master thread does [%zu, %zu).\n", num_threads, threadIdx, begin, end);

    for (timestep_count = 0; !terminateFlag; ++timestep_count) {
        /* LEAPFROG 1 */
        {
            std::lock_guard<std::mutex> lk(cv_m);
            wait_flag |= (1ULL << 0);
        }
        cv.notify_all();

        leapfrog1(end - begin, 0.01, x + 2 * begin, v + 2 * begin, a + 2 * begin);

        {
            std::unique_lock<std::mutex> lk(cv_m);
            if (num_threads == 2)
                cv.wait(lk, [this] { return wait_flag == 0x03; });
            else
                cv.wait(lk, [this] { return wait_flag == 0x0F; });
        }

        /* APPLY REFLECT */
        {
            std::lock_guard<std::mutex> lk(cv_m);
            wait_flag &= ~(1ULL << 0);
        }
        cv.notify_all();

        apply_reflect(end - begin, x + 2 * begin, v + 2 * begin, a + 2 * begin);

        {
            std::unique_lock<std::mutex> lk(cv_m);
            if (num_threads == 2)
                cv.wait(lk, [this] { return !(wait_flag & 0x03); });
            else
                cv.wait(lk, [this] { return !(wait_flag & 0x0F); });
        }

        /* COMPUTE */
        {
            std::lock_guard<std::mutex> lk(cv_m);
            wait_flag |= (1ULL << 0);
        }
        cv.notify_all();

        for (size_t i = 0; i < n; ++i) {
            aLocal[threadIdx][2 * i + 0] = 0.f;
            aLocal[threadIdx][2 * i + 1] = 0.f;
        }
        /* Particle-particle interactions (Lennard-Jones) */
        for (size_t i = threadIdx; i < n; i += num_threads) {
            for (size_t j = i + 1; j < n; ++j) {
                float dx = x[2 * j + 0] - x[2 * i + 0];
                float dy = x[2 * j + 1] - x[2 * i + 1];
                float C_LJ = compute_LJ_scalar(dx * dx / 1000 + dy * dy / 1000, LJ_EPS, LJ_SIG2);
                if (isnan(C_LJ)) C_LJ = 0;
                if (C_LJ > 100) C_LJ = 100;
                if (C_LJ < -100) C_LJ = -100;
                aLocal[threadIdx][2 * i + 0] += (C_LJ * dx);
                aLocal[threadIdx][2 * i + 1] += (C_LJ * dy);
                aLocal[threadIdx][2 * j + 0] -= (C_LJ * dx);
                aLocal[threadIdx][2 * j + 1] -= (C_LJ * dy);
            }
        }

        {
            std::unique_lock<std::mutex> lk(cv_m);
            if (num_threads == 2)
                cv.wait(lk, [this] { return wait_flag == 0x03; });
            else
                cv.wait(lk, [this] { return wait_flag == 0x0F; });
        }

        /* Reduce "aLocals" into "a" and add global force downward (e.g. gravity) */
        for (size_t i = 0; i < n; ++i) {
            a[2 * i + 0] = 0.f;
            a[2 * i + 1] = LJ_G;
            for (size_t t = 0; t < num_threads; ++t) {
                a[2 * i + 0] += aLocal[t][2 * i + 0];
                a[2 * i + 1] += aLocal[t][2 * i + 1];
            }
        }

        /* LEAPFROG 2 */
        {
            std::lock_guard<std::mutex> lk(cv_m);
            wait_flag &= ~(1ULL << 0);
        }
        cv.notify_all();

        leapfrog2(end - begin, 0.01, v + 2 * begin, a + 2 * begin);

        {
            std::unique_lock<std::mutex> lk(cv_m);
            if (num_threads == 2)
                cv.wait(lk, [this] { return !(wait_flag & 0x03); });
            else
                cv.wait(lk, [this] { return !(wait_flag & 0x0F); });
        }

        ++timestep_count;
        ++engine->realSimStepsPerSecond;
    }
}

void SimThreadMultiLJPotential::ResetParticle(size_t i)
{
    if (i < 1000)
        vertices[i].color = sf::Color::Red;
    else
        vertices[i].color = sf::Color::Green;

    x[2 * i + 0] = 300.f + (i / 40) * 4.f;
    x[2 * i + 1] = 320.f + (i % 40) * 4.f;
    v[2 * i + 0] = 0.f;
    v[2 * i + 1] = 0.f;
    a[2 * i + 0] = 0.f;
    a[2 * i + 1] = 0.f;
}
