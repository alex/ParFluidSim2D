#include "SimThreadRenderLJPotential.h"

#include "LennardJones.h"
#include "Sim.h"

#include <SFML/Window.hpp>

#include <string>
#include <thread>
#include <type_traits>

#include <math.h>
#include <stdio.h>

SimThreadRenderLJPotential::SimThreadRenderLJPotential(FluidSimEngine* engine, const std::string& name, size_t count)
    : Sim(engine, name)
    , calc_thread(&SimThreadRenderLJPotential::Compute, this)
    , bounds(sf::Vector2f(200.f, 230.f))
    , vertices(sf::Points, count)
    , n(count)
    , x(new float[2 * count])
    , v(new float[2 * count])
    , a(new float[2 * count])
    , terminateFlag(false)
    , timestep_count(0ULL)
    , timestep_rendered(0ULL)
{
    bounds.setFillColor(sf::Color(16, 16, 16));
    bounds.setPosition(300.f, 240.f);

    for (size_t i = 0; i < n; ++i) {
        ResetParticle(i);
    }
}

SimThreadRenderLJPotential::~SimThreadRenderLJPotential()
{
    terminateFlag = true;
    calc_thread.join();

    delete[] x;
    delete[] v;
    delete[] a;
}

void SimThreadRenderLJPotential::HandleTap(const int x, const int y)
{ }

void SimThreadRenderLJPotential::HandleKey(const sf::Event::KeyEvent& key)
{
    if (key.code == sf::Keyboard::Backspace) {
        this->engine->PopSim();
    }
}

bool SimThreadRenderLJPotential::Update(float delta)
{
    // update the position of the corresponding vertex
    for (size_t i = 0; i < n; ++i) {
        vertices[i].position.x = x[2 * i + 0];
        vertices[i].position.y = x[2 * i + 1];
    }

    return timestep_count > timestep_rendered;
}

void SimThreadRenderLJPotential::Render(sf::RenderWindow& window, float delta)
{
    timestep_rendered = timestep_count;
    window.draw(bounds);
    window.draw(vertices);
}

void SimThreadRenderLJPotential::Compute()
{
    for (timestep_count = 0; !terminateFlag; ++timestep_count) {
        leapfrog1(n, 0.01, x, v, a);
        apply_reflect(n, x, v, a);
        compute_forces();
        leapfrog2(n, 0.01, v, a);
        ++engine->realSimStepsPerSecond;
    }
}

void SimThreadRenderLJPotential::compute_forces()
{
    /* Global force downward (e.g. gravity) */
    for (size_t i = 0; i < n; ++i) {
        a[2 * i + 0] = 0.f;
        a[2 * i + 1] = LJ_G;
    }

    /* Particle-particle interactions (Lennard-Jones) */
    for (size_t i = 0; i < n; ++i) {
        for (size_t j = i + 1; j < n; ++j) {
            float dx = x[2 * j + 0] - x[2 * i + 0];
            float dy = x[2 * j + 1] - x[2 * i + 1];
            float C_LJ = compute_LJ_scalar(dx * dx / 1000 + dy * dy / 1000, LJ_EPS, LJ_SIG2);
            if (isnan(C_LJ)) C_LJ = 0;
            if (C_LJ > 100) C_LJ = 100;
            if (C_LJ < -100) C_LJ = -100;
            a[2 * i + 0] += (C_LJ * dx);
            a[2 * i + 1] += (C_LJ * dy);
            a[2 * j + 0] -= (C_LJ * dx);
            a[2 * j + 1] -= (C_LJ * dy);
        }
    }
}

void SimThreadRenderLJPotential::ResetParticle(size_t i)
{
    x[2 * i + 0] = 300.f + (i / 40) * 4.f;
    x[2 * i + 1] = 320.f + (i % 40) * 4.f;
    v[2 * i + 0] = 0.f;
    v[2 * i + 1] = 0.f;
    a[2 * i + 0] = 0.f;
    a[2 * i + 1] = 0.f;
}
