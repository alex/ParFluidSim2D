#include "FluidSimEngine.h"
#include "SimMainMenu.h"

#include <iostream>
#include <string>

#include <stdlib.h>

int main(int argc, char* argv[])
{
    std::cout << PROJ_NAME << "\n";

    FluidSimEngine *GEngine = new FluidSimEngine();
    SimMainMenu *sim = new SimMainMenu(GEngine, "MainMenu");

    if (GEngine->Init()) return 1;

    GEngine->AddSim(sim);
    GEngine->MainLoop();

    delete GEngine; // sim is deleted by GEngine during Shutdown()

    return 0;
}
