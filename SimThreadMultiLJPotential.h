#pragma once

#include "Sim.h"
#include "FluidSimEngine.h"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <condition_variable>
#include <mutex>
#include <thread>
#include <vector>

class SimThreadMultiLJPotential : public Sim
{
public:
    SimThreadMultiLJPotential(FluidSimEngine*, const std::string&, const size_t, const size_t);
    ~SimThreadMultiLJPotential() override;
    void HandleTap(const int, const int) override;
    void HandleKey(const sf::Event::KeyEvent&) override;
    bool Update(float) override;
    void Render(sf::RenderWindow&, float) override;

private:
    void Compute(const size_t);
    void ComputeMaster(const size_t);
    void ResetParticle(size_t);

    std::condition_variable cv;
    std::mutex cv_m;
    unsigned int wait_flag;
    std::vector<std::thread> calc_threads;
    size_t num_threads;

    sf::RectangleShape bounds;
    sf::VertexArray vertices;
    size_t n;
    float* x;
    float* v;
    float* a;
    float** aLocal;

    bool terminateFlag;
    size_t timestep_count;
    size_t timestep_rendered;
};
