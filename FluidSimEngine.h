#pragma once

#include "Sim.h"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <stack>

// BadgeEngine handles input and rendering loops
class FluidSimEngine
{
public:
    FluidSimEngine();
    ~FluidSimEngine();

    int Init();
    void AddSim(Sim*);
    void PopSim();

    void MainLoop();

    unsigned int realSimStepsPerSecond;

private:
    const sf::Vector2u size;
    const std::string title;

    sf::RenderWindow window;
    sf::Text textFPS[3];

    std::stack<Sim*> programs;
};
