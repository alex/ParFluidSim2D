#pragma once

#include "Sim.h"
#include "FluidSimEngine.h"

#include <SFML/Window.hpp>

class SimMainMenu : public Sim
{
public:
    SimMainMenu(FluidSimEngine*, const std::string&);
    ~SimMainMenu() override;
    void HandleTap(const int, const int) override;
    void HandleKey(const sf::Event::KeyEvent&) override;
    bool Update(float) override;
    void Render(sf::RenderWindow&, float) override;

private:
    sf::Text textTitle;
    sf::Text textCoursename;
    sf::Text textAuthor;
    sf::Text textDate;
    sf::Text textVersion;
    sf::Text textMainMenuHeading;
    sf::Text selectionIndicator;
    sf::Text mainMenuListing;
    sf::RectangleShape mainMenuBox;
    std::string selectionString;
    std::vector<std::string> menuItems;

    unsigned int currentSelection;
    unsigned int lastSelection;
};
