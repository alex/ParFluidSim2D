CXX=g++
CFLAGS=-g -Wall -DPROJ_NAME=\"ParFluidSim2D\"
LIBS=-lsfml-graphics -lsfml-window -lsfml-system -lpthread

DEPS = CourierFont.h FluidSimEngine.h LennardJones.h Sim.h SimMainMenu.h SimSingleLJPotential.h SimThreadMultiLJPotential.h SimThreadRenderLJPotential.h
OBJ = FluidSimEngine.obj SimMainMenu.obj SimSingleLJPotential.obj SimThreadMultiLJPotential.obj SimThreadRenderLJPotential.obj main.obj

%.obj: %.cpp $(DEPS)
	$(CXX) -c -o $@ $< $(CFLAGS)

ParFluidSim2D: $(OBJ)
	$(CXX) -o $@ $^ $(CFLAGS) $(LIBS)
	mkdir bin && mv $@ bin/

.PHONY: clean

clean:
	rm -rvf *.obj bin
