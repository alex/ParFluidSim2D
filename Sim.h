#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <string>

class FluidSimEngine;

class Sim
{
public:
    FluidSimEngine *const engine;
   
    const std::string name;

    Sim(FluidSimEngine* _engine, const std::string& _name) : engine(_engine), name(_name) {}
    virtual ~Sim() {}
    virtual void HandleTap(const int, const int) = 0;
    virtual void HandleKey(const sf::Event::KeyEvent&) = 0;
    virtual bool Update(float) = 0;
    virtual void Render(sf::RenderWindow&, float) = 0;
};
