#pragma once

static constexpr float LJ_G = 1.f;
static constexpr float LJ_EPS = 1e-2f;
static constexpr float LJ_SIG = 0.1f;
static constexpr float LJ_SIG2 = LJ_SIG * LJ_SIG;

static constexpr float XMIN = 300.f;
static constexpr float XMAX = 500.f;
static constexpr float YMIN = 240.f;
static constexpr float YMAX = 470.f;

inline float compute_LJ_scalar(float r2, float eps, float sig2)
{
    if (r2 < 6.25 * sig2) { /* r_cutoff = 2.5 sigma */
        float z = sig2 / r2;
        float u = z * z * z;
        return 24 * eps / r2 * u * (1 - 2 * u);
    }
    return 0;
}

inline float potential_LJ(float r2, float eps, float sig2)
{
    float z = sig2 / r2;
    float u = z * z * z;
    return 4 * eps * u * (1 - u);
}

inline void leapfrog1(size_t n, float dt, float* __restrict x, float* __restrict v, float* __restrict a)
{
    for (size_t i = 0; i < n; ++i, x += 2, v += 2, a += 2) {
        v[0] += a[0] * dt / 2;
        v[1] += a[1] * dt / 2;
        x[0] += v[0] * dt;
        x[1] += v[1] * dt;
    }
}

inline void leapfrog2(size_t n, float dt, float* __restrict v, float* __restrict a)
{
    for (size_t i = 0; i < n; ++i, v += 2, a += 2) {
        v[0] += a[0] * dt / 2;
        v[1] += a[1] * dt / 2;
    }
}

static inline void reflect(float wall, float* __restrict x, float* __restrict v, float* __restrict a)
{
    *x = (2 * wall - (*x));
    *v = -(*v);
    *a = -(*a);
}

inline void apply_reflect(size_t n, float* __restrict x, float* __restrict v, float* __restrict a)
{
    for (size_t i = 0; i < n; ++i, x += 2, v += 2, a += 2) {
        if (x[0] < XMIN) reflect(XMIN, x + 0, v + 0, a + 0);
        if (x[0] > XMAX) reflect(XMAX, x + 0, v + 0, a + 0);
        if (x[1] < YMIN) reflect(YMIN, x + 1, v + 1, a + 1);
        if (x[1] > YMAX) reflect(YMAX, x + 1, v + 1, a + 1);
    }
}
