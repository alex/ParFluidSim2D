#include "FluidSimEngine.h"

#include "CourierFont.h"
#include "Sim.h"

#include <iostream>
#include <stack>
#include <string>

#include <stdio.h>

sf::Font cour;
sf::Font courbd;
sf::Font courbi;
sf::Font couri;

#define currentSim programs.top()

FluidSimEngine::FluidSimEngine()
    : size(sf::Vector2u(800, 480))
    , title(PROJ_NAME)
    , realSimStepsPerSecond(0)
{
    window.create(sf::VideoMode(size.x, size.y), title, sf::Style::Titlebar | sf::Style::Close);
}

FluidSimEngine::~FluidSimEngine()
{
    std::cout << "Shutting down..." << std::endl;
    while (!programs.empty())
    {
        this->PopSim();
    }
}

int FluidSimEngine::Init()
{
    bool fontLoadError = false;
    if (!cour  .loadFromMemory(cour_ttf,   cour_ttf_len))   std::cerr << "ERROR: failed to load font cour"   << std::endl, fontLoadError = true;
    if (!courbd.loadFromMemory(courbd_ttf, courbd_ttf_len)) std::cerr << "ERROR: failed to load font courbd" << std::endl, fontLoadError = true;
    if (!courbi.loadFromMemory(courbi_ttf, courbi_ttf_len)) std::cerr << "ERROR: failed to load font courbi" << std::endl, fontLoadError = true;
    if (!couri .loadFromMemory(couri_ttf,  couri_ttf_len))  std::cerr << "ERROR: failed to load font couri"  << std::endl, fontLoadError = true;
    if (fontLoadError)
        return -1;

    for (size_t c = 0; c < 3; ++c) {
        textFPS[c].setFont(courbd);
        textFPS[c].setCharacterSize(14);
        textFPS[c].setFillColor(sf::Color(255, 255, 255, 127));
        textFPS[c].setPosition(700.f, 10.f + (c * 15.f));
    }

    // Options for limiting FPS, if needed
    window.setVerticalSyncEnabled(false);
    //window.setFramerateLimit(60);

    return 0;
}

void FluidSimEngine::AddSim(Sim* sim)
{
    std::cout << "[\033[33mENGINE\033[0m][ADD SIM ]: " << sim->name << std::endl;
    programs.push(sim);
}

void FluidSimEngine::PopSim()
{
    std::cout << "[\033[33mENGINE\033[0m][DEL SIM ]: " << programs.top()->name << std::endl;
    Sim* sim = programs.top();
    programs.pop();
    delete sim;
}

void FluidSimEngine::MainLoop()
{
    sf::Clock clock;

    unsigned int fpsCounter = 0;
    float lastFPSTime = 0.0f;

    float lastTime = 0.0f;
    float currentTime = 0.0f;
    float delta;

    while (window.isOpen()) {
        // check all events in the queue
        sf::Event event;
        while (window.pollEvent(event)) {
            switch (event.type) {
            case sf::Event::Closed:
                window.close();
                break;
            case sf::Event::MouseButtonPressed:
                printf("[\033[33mENGINE\033[0m][Mouse   ]: %d, %d\n", event.mouseButton.x, event.mouseButton.y);
                currentSim->HandleTap(event.mouseButton.x, event.mouseButton.y);
                break;
            case sf::Event::KeyPressed:
                printf("[\033[33mENGINE\033[0m][KeyPress]: %u\n", (unsigned int)event.key.code);
                switch (event.key.code) {
                case sf::Keyboard::Escape:
                case sf::Keyboard::Q:
                    if (programs.size() > 1)
                        this->PopSim();
                    else
                        window.close();
                    break;
                default:
                    currentSim->HandleKey(event.key);
                }
                break;
            default:
                break;
            }
        }

        currentTime = clock.getElapsedTime().asSeconds();
        delta = currentTime - lastTime;
        lastTime = currentTime;
        textFPS[0].setString(std::string("d: ") + std::to_string(delta));

        if (currentSim->Update(delta)) {
            window.clear(sf::Color::Black);
            currentSim->Render(window, delta);

            // draw fps counter overlay and display final image
            window.draw(textFPS[0]);
            window.draw(textFPS[1]);
            window.draw(textFPS[2]);
            window.display();
        }

        ++fpsCounter;
        if (currentTime - lastFPSTime >= 1.0f) {
            std::cout << "[\033[33mENGINE\033[0m][CurrTime]: " << currentTime << ". FPS: " << fpsCounter << " SimPS: " << realSimStepsPerSecond << std::endl;
            textFPS[1].setString(std::string("FPS: ") + std::to_string(fpsCounter));
            fpsCounter = 0;
            lastFPSTime = (unsigned int)currentTime;
            textFPS[2].setString(std::string("SPS: ") + std::to_string(realSimStepsPerSecond));
            realSimStepsPerSecond = 0;
        }
    }
}
