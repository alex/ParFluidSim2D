#pragma once

#include "Sim.h"
#include "FluidSimEngine.h"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <thread>

class SimThreadRenderLJPotential : public Sim
{
public:
    SimThreadRenderLJPotential(FluidSimEngine*, const std::string&, size_t);
    ~SimThreadRenderLJPotential() override;
    void HandleTap(const int, const int) override;
    void HandleKey(const sf::Event::KeyEvent&) override;
    bool Update(float) override;
    void Render(sf::RenderWindow&, float) override;

private:
    void Compute();
    void compute_forces();
    void ResetParticle(size_t);

    std::thread calc_thread;

    sf::RectangleShape bounds;
    sf::VertexArray vertices;
    size_t n;
    float* x;
    float* v;
    float* a;

    bool terminateFlag;
    size_t timestep_count;
    size_t timestep_rendered;
};
