#include "SimMainMenu.h"

#include "Sim.h"
#include "SimSingleLJPotential.h"
#include "SimThreadMultiLJPotential.h"
#include "SimThreadRenderLJPotential.h"

#include <SFML/Window.hpp>

#include <iostream>
#include <numeric>
#include <string>

#define NUM_PARTICLES 2'000

extern sf::Font cour;
extern sf::Font courbd;
extern sf::Font courbi;
extern sf::Font couri;

SimMainMenu::SimMainMenu(FluidSimEngine* engine, const std::string& name)
    : Sim(engine, name)
    , currentSelection(0)
    , lastSelection(-1)
{
    menuItems.push_back("L-J Potential, Single\n");
    menuItems.push_back("L-J Potential, Separate Compute/Render\n");
    menuItems.push_back("L-J Potential, 2 Compute Threads\n");
    menuItems.push_back("L-J Potential, 4 Compute Threads\n");

    textTitle.setFont(courbd);
    textTitle.setString(PROJ_NAME);
    textTitle.setPosition(100, 0);
    textTitle.setCharacterSize(48);
    textTitle.setFillColor(sf::Color::White);

    textCoursename.setFont(cour);
    textCoursename.setString("textCourseName");
    textCoursename.setPosition(100, 54);
    textCoursename.setCharacterSize(15);
    textCoursename.setFillColor(sf::Color(192, 192, 192));

    textAuthor.setFont(couri);
    textAuthor.setString("by textAuthor");
    textAuthor.setPosition(100, 445);
    textAuthor.setCharacterSize(15);
    textAuthor.setFillColor(sf::Color(128, 128, 128));

    textDate.setFont(courbd);
    textDate.setString("200319");
    textDate.setPosition(700, 450);
    textDate.setCharacterSize(15);
    textDate.setFillColor(sf::Color::White);

    textVersion.setFont(courbd);
    textVersion.setString("v0.1");
    textVersion.setPosition(485, 35);
    textVersion.setCharacterSize(13);
    textVersion.setFillColor(sf::Color::White);

    textMainMenuHeading.setFont(courbi);
    textMainMenuHeading.setString("Main Menu");
    textMainMenuHeading.setPosition(180, 100);
    textMainMenuHeading.setCharacterSize(30);
    textMainMenuHeading.setFillColor(sf::Color::Black);

    selectionIndicator.setFont(courbd);
    selectionIndicator.setString(selectionString);
    selectionIndicator.setPosition(150, 150);
    selectionIndicator.setCharacterSize(18);
    selectionIndicator.setFillColor(sf::Color::Black);

    mainMenuListing.setFont(cour);
    mainMenuListing.setString(std::accumulate(menuItems.begin(), menuItems.end(), std::string("")));
    mainMenuListing.setPosition(180, 150);
    mainMenuListing.setCharacterSize(18);
    mainMenuListing.setFillColor(sf::Color::Black);

    mainMenuBox.setPosition(120, 90);
    mainMenuBox.setSize(sf::Vector2f(500, 300));
    mainMenuBox.setFillColor(sf::Color::White);
    mainMenuBox.setOutlineColor(sf::Color(128, 128, 128));
    mainMenuBox.setOutlineThickness(3);
}

SimMainMenu::~SimMainMenu()
{ }

void SimMainMenu::HandleTap(const int _x, const int _y)
{ }

void SimMainMenu::HandleKey(const sf::Event::KeyEvent& key)
{
    if (key.code == sf::Keyboard::Up)
        if (currentSelection > 0)
            --currentSelection;
    if (key.code == sf::Keyboard::Down)
        if (currentSelection < menuItems.size() - 1)
            ++currentSelection;

    if (key.code == sf::Keyboard::Enter) {
        if (currentSelection == 0) {
            printf("[\033[32mSIM:MM\033[0m][SwitchSS]: SimSingleLJPotential\n");
            Sim* sim = new SimSingleLJPotential(engine, "SimSingleLJPotential", NUM_PARTICLES);
            this->engine->AddSim(sim);
        } else if (currentSelection == 1) {
            printf("[\033[32mSIM:MM\033[0m][SwitchSS]: SimThreadRenderLJPotential\n");
            Sim* sim = new SimThreadRenderLJPotential(engine, "SimThreadRenderLJPotential", NUM_PARTICLES);
            this->engine->AddSim(sim);
        } else if (currentSelection == 2) {
            printf("[\033[32mSIM:MM\033[0m][SwitchSS]: SimThreadMultiLJPotential: 2 Threads\n");
            Sim* sim = new SimThreadMultiLJPotential(engine, "SimThreadMultiLJPotential: 2", NUM_PARTICLES, 2);
            this->engine->AddSim(sim);
        } else if (currentSelection == 3) {
            printf("[\033[32mSIM:MM\033[0m][SwitchSS]: SimThreadMultiLJPotential: 4 Threads\n");
            Sim* sim = new SimThreadMultiLJPotential(engine, "SimThreadMultiLJPotential: 4", NUM_PARTICLES, 4);
            this->engine->AddSim(sim);
        }

        lastSelection = -1;
    }
}

bool SimMainMenu::Update(float delta)
{
    if (currentSelection == lastSelection)
        return false;

    printf("[\033[32mSIM:MM\033[0m][Selected]: %u\n", currentSelection);

    selectionString = "";
    for (unsigned int i = 0; i < currentSelection; ++i)
        selectionString += "\n";
    selectionString += ">";
    selectionIndicator.setString(selectionString);

    lastSelection = currentSelection;

    return true;
}

void SimMainMenu::Render(sf::RenderWindow& window, float delta)
{
    std::cout << "[\033[32mSIM:MM\033[0m]SimMainMenu::Render" << std::endl;

    window.draw(mainMenuBox);
    window.draw(textTitle);
    window.draw(textCoursename);
    window.draw(textAuthor);
    window.draw(textDate);
    window.draw(textVersion);
    window.draw(textMainMenuHeading);
    window.draw(selectionIndicator);
    window.draw(mainMenuListing);
}
